import math

def calc():
    print("\tДействия\n\n")
    print("1 - сложение\n")
    print("2 - вычитание\n")
    print("3 - умножение\n")
    print("4 - деление\n")
    print("5 - деление нацело\n")
    print("6 - нахождение остатка от деления\n")
    print("7 - степень\n")

    # math lib
    print("8 - округление до ближайшего большего числа\n")
    print("9 - модуль\n")
    print("10 - факториал числа\n")
    print("11 - округление вниз\n")
    print("12 - расчёт площади параллелепипеда\n")
    print("13 - расчёт периметра параллелепипеда\n")
    print("0 - выход к выбору")

    while True:
        try:
            deistvie_calc = int(input("\n\nВыберите действие: "))
            break
        except:
            print("\n\n\tОШИБКА (введите цифру!)\t")

    if deistvie_calc == 0:
        calc()

    if deistvie_calc == 1:
        a = 0
        b = 0

        while True:
            try:
                a = float(input("\n\nВведите первое число: "))
                b = float(input("\nВведите второе число: "))
                break
            except:
                print("\n\n\tОШИБКА (введите цифру!)\t")

        res = a + b
        print("\nРезультат: ", res)

        try:
            vihod = int(input("\n\nДля выхода введите 0: "))

            while vihod != 0:
                vihod = int(input("\n\nДля выхода введите 0: "))

            calc()
        except:
            print("\n\t\tОШИБКА! ВЫХОД В ГЛАВНОЕ МЕНЮ\n\n")
            calc()

    if deistvie_calc == 2:
        a = 0
        b = 0

        while True:
            try:
                a = float(input("\n\nВведите первое число: "))
                b = float(input("\nВведите второе число: "))
                break
            except:
                print("\n\n\tОШИБКА (введите цифру!)\t")

        res = a - b
        print("\nРезультат: ", res)

        try:
            vihod = int(input("\n\nДля выхода введите 0: "))

            while vihod != 0:
                vihod = int(input("\n\nДля выхода введите 0: "))

            calc()
        except:
            print("\n\t\tОШИБКА! ВЫХОД В ГЛАВНОЕ МЕНЮ\n\n")
            calc()

    if deistvie_calc == 3:
        a = 0
        b = 0

        while True:
            try:
                a = float(input("\n\nВведите первое число: "))
                b = float(input("\nВведите второе число: "))
                break
            except:
                print("\n\n\tОШИБКА (введите цифру!)\t")

        res = a * b
        print("\nРезультат: ", res)

        try:
            vihod = int(input("\n\nДля выхода введите 0: "))

            while vihod != 0:
                vihod = int(input("\n\nДля выхода введите 0: "))

            calc()
        except:
            print("\n\t\tОШИБКА! ВЫХОД В ГЛАВНОЕ МЕНЮ\n\n")
            calc()

    if deistvie_calc == 4:
        a = 0
        b = 0

        while True:
            try:
                a = float(input("\n\nВведите первое число: "))
                b = float(input("\nВведите второе число: "))
                break
            except:
                print("\n\n\tОШИБКА (введите цифру!)\t")

        res = a / b
        print("\nРезультат: ", res)

        try:
            vihod = int(input("\n\nДля выхода введите 0: "))

            while vihod != 0:
                vihod = int(input("\n\nДля выхода введите 0: "))

            calc()
        except:
            print("\n\t\tОШИБКА! ВЫХОД В ГЛАВНОЕ МЕНЮ\n\n")
            calc()

    if deistvie_calc == 5:
        a = 0
        b = 0

        while True:
            try:
                a = float(input("\n\nВведите первое число: "))
                b = float(input("\nВведите второе число: "))
                break
            except:
                print("\n\n\tОШИБКА (введите цифру!)\t")

        res = a // b
        print("\nРезультат: ", res)

        try:
            vihod = int(input("\n\nДля выхода введите 0: "))

            while vihod != 0:
                vihod = int(input("\n\nДля выхода введите 0: "))

            calc()
        except:
            print("\n\t\tОШИБКА! ВЫХОД В ГЛАВНОЕ МЕНЮ\n\n")
            calc()

    if deistvie_calc == 6:
        a = 0
        b = 0

        while True:
            try:
                a = float(input("\n\nВведите первое число: "))
                b = float(input("\nВведите второе число: "))
                break
            except:
                print("\n\n\tОШИБКА (введите цифру!)\t")

        res = a % b
        print("\nРезультат: ", res)

        try:
            vihod = int(input("\n\nДля выхода введите 0: "))

            while vihod != 0:
                vihod = int(input("\n\nДля выхода введите 0: "))

            calc()
        except:
            print("\n\t\tОШИБКА! ВЫХОД В ГЛАВНОЕ МЕНЮ\n\n")
            calc()

    if deistvie_calc == 7:
        a = 0
        b = 0

        while True:
            try:
                a = float(input("\n\nВведите первое число: "))
                b = float(input("\nВведите второе число: "))
                break
            except:
                print("\n\n\tОШИБКА (введите цифру!)\t")

        res = a ** b
        print("\nРезультат: ", res)

        try:
            vihod = int(input("\n\nДля выхода введите 0: "))

            while vihod != 0:
                vihod = int(input("\n\nДля выхода введите 0: "))

            calc()
        except:
            print("\n\t\tОШИБКА! ВЫХОД В ГЛАВНОЕ МЕНЮ\n\n")
            calc()

    if deistvie_calc == 8:
        a = 0

        while True:
            try:
                a = float(input("\n\nВведите число: "))
                break
            except:
                print("\n\n\tОШИБКА (введите вещественное число!)\t")

        res = math.ceil(a)
        print("\nРезультат: ", res)

        try:
            vihod = int(input("\n\nДля выхода введите 0: "))

            while vihod != 0:
                vihod = int(input("\n\nДля выхода введите 0: "))

            calc()
        except:
            print("\n\t\tОШИБКА! ВЫХОД В ГЛАВНОЕ МЕНЮ\n\n")
            calc()

    if deistvie_calc == 9:
        a = 0

        while True:
            try:
                a = float(input("\n\nВведите число: "))
                break
            except:
                print("\n\n\tОШИБКА (введите цифру!)\t")

        res = math.fabs(a)
        print("\nРезультат: ", res)

        try:
            vihod = int(input("\n\nДля выхода введите 0: "))

            while vihod != 0:
                vihod = int(input("\n\nДля выхода введите 0: "))

            calc()
        except:
            print("\n\t\tОШИБКА! ВЫХОД В ГЛАВНОЕ МЕНЮ\n\n")
            calc()

    if deistvie_calc == 10:
        a = 0

        while True:
            try:
                a = int(input("\n\nВведите число: "))
                break
            except:
                print("\n\n\tОШИБКА (введите целое число!)\t")

        res = math.factorial(a)
        print("\nРезультат: ", res)

        try:
            vihod = int(input("\n\nДля выхода введите 0: "))

            while vihod != 0:
                vihod = int(input("\n\nДля выхода введите 0: "))

            calc()
        except:
            print("\n\t\tОШИБКА! ВЫХОД В ГЛАВНОЕ МЕНЮ\n\n")
            calc()

    if deistvie_calc == 11:
        a = 0

        while True:
            try:
                a = float(input("\n\nВведите число: "))
                break
            except:
                print("\n\n\tОШИБКА (введите вещественное число!)\t")

        res = math.floor(a)
        print("\nРезультат: ", res)

        try:
            vihod = int(input("\n\nДля выхода введите 0: "))

            while vihod != 0:
                vihod = int(input("\n\nДля выхода введите 0: "))

            calc()
        except:
            print("\n\t\tОШИБКА! ВЫХОД В ГЛАВНОЕ МЕНЮ\n\n")
            calc()

    if deistvie_calc == 12:
        a = 0
        b = 0
        c = 0

        while True:
            try:
                a = float(input("\n\nДлина ребра а (фронтальное): "))
                b = float(input("\nДлина ребра b (боковое): "))
                c = float(input("\nДлина ребра c (высота): "))
                break
            except:
                print("\n\n\tОШИБКА (введите цифру!)\t")

        res = lambda a_, b_, c_: 2*(c*a + a*b + c*b)
        print("\nРезультат: ", res(a,b,c), "(м^2)")

        try:
            vihod = int(input("\n\nДля выхода введите 0: "))

            while vihod != 0:
                vihod = int(input("\n\nДля выхода введите 0: "))

            calc()
        except:
            print("\n\t\tОШИБКА! ВЫХОД В ГЛАВНОЕ МЕНЮ\n\n")
            calc()

    if deistvie_calc == 13:
        a = 0
        b = 0
        c = 0

        while True:
            try:
                a = float(input("\n\nДлина ребра а (фронтальное): "))
                b = float(input("\nДлина ребра b (боковое): "))
                c = float(input("\nДлина ребра c (высота): "))
                break
            except:
                print("\n\n\tОШИБКА (введите цифру!)\t")

        res = lambda a_, b_, c_: 4*a_ + 4*b_ + 4*c_
        print("\nРезультат: ", res(a,b,c))

        try:
            vihod = int(input("\n\nДля выхода введите 0: "))

            while vihod != 0:
                vihod = int(input("\n\nДля выхода введите 0: "))

            calc()
        except:
            print("\n\t\tОШИБКА! ВЫХОД В ГЛАВНОЕ МЕНЮ\n\n")
            calc()

def stroki():
    a = (input("\n\nВведите строку: "))

    res_sybmlos = len(a)
    res_probelov = len(a) - len(a.replace(" ", ""))

    start = -1
    res_zyapyatih = 0

    while True:
        start = a.find(',', start+1)
        if start == -1:
            break
        res_zyapyatih += 1

    print("\n\nКол-во символов: ", res_sybmlos)
    print("\n\nКол-во пробелов: ", res_probelov)
    print("\n\nКол-во запятых: ", res_zyapyatih)

    try:
        vihod = int(input("\n\nДля выхода введите 0: "))

        while vihod != 0:
            vihod = int(input("\n\nДля выхода введите 0: "))

        calc()
    except:
        print("\n\t\tОШИБКА! ВЫХОД В ГЛАВНОЕ МЕНЮ\n\n")
        calc()
calc()