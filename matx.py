def matrix():

    x = 0
    y = 0
    a = 0
    b = 0

    try:
        x = int(input("\n\nВведите кол-во столбцов: "))
        y = int(input("\nВведите кол-во строк: "))
        a = int(input("\nВведите начальное значение: "))
        b = int(input("\nВведите шаг (на сколько будет увеличиваться значение): "))
    except:
        print("\n\n\tОШИБКА (введите целые цифры!)\t")
        matrix()

    matrix_ = []

    for row in range(y):
        temp_matrix = []
        for column in range(x):
            temp_matrix.append(a)
            a += b
        matrix_.append(temp_matrix)

    print("\n\nВаша матрица: \n")

    for i in range(y):
        for j in range(x):
            print(matrix_[i][j], end=" ")
        print()

    try:
        vihod = int(input("\n\nДля выхода введите 0: "))

        while vihod != 0:
            vihod = int(input("\n\nДля выхода введите 0: "))

        matrix()
    except:
        print("\n\t\tОШИБКА! ВЫХОД В ГЛАВНОЕ МЕНЮ\n\n")
        matrix()
matrix()