import math

def calc():
    print("\tДействия\n\n")
    print("1 - сложение\n")
    print("2 - вычитание\n")
    print("3 - умножение\n")
    print("4 - деление\n")
    print("5 - деление нацело\n")
    print("6 - нахождение остатка от деления\n")
    print("7 - степень\n")

    # math lib
    print("8 - округление до ближайшего большего числа\n")
    print("9 - модуль\n")
    print("10 - факториал числа\n")
    print("11 - округление вниз\n")
    print("12 - расчёт площади параллелепипеда\n")
    print("13 - расчёт периметра параллелепипеда\n")
    print("0 - выход к выбору")

    while True:
        try:
            deistvie_calc = int(input("\n\nВыберите действие: "))
            break
        except:
            print("\n\n\tОШИБКА (введите цифру!)\t")

    if deistvie_calc == 0:
        main()

    if deistvie_calc == 1:
        a = 0
        b = 0

        while True:
            try:
                a = float(input("\n\nВведите первое число: "))
                b = float(input("\nВведите второе число: "))
                break
            except:
                print("\n\n\tОШИБКА (введите цифру!)\t")

        res = a + b
        print("\nРезультат: ", res)

        try:
            vihod = int(input("\n\nДля выхода введите 0: "))

            while vihod != 0:
                vihod = int(input("\n\nДля выхода введите 0: "))

            calc()
        except:
            print("\n\t\tОШИБКА! ВЫХОД В ГЛАВНОЕ МЕНЮ\n\n")
            main()

    if deistvie_calc == 2:
        a = 0
        b = 0

        while True:
            try:
                a = float(input("\n\nВведите первое число: "))
                b = float(input("\nВведите второе число: "))
                break
            except:
                print("\n\n\tОШИБКА (введите цифру!)\t")

        res = a - b
        print("\nРезультат: ", res)

        try:
            vihod = int(input("\n\nДля выхода введите 0: "))

            while vihod != 0:
                vihod = int(input("\n\nДля выхода введите 0: "))

            calc()
        except:
            print("\n\t\tОШИБКА! ВЫХОД В ГЛАВНОЕ МЕНЮ\n\n")
            main()

    if deistvie_calc == 3:
        a = 0
        b = 0

        while True:
            try:
                a = float(input("\n\nВведите первое число: "))
                b = float(input("\nВведите второе число: "))
                break
            except:
                print("\n\n\tОШИБКА (введите цифру!)\t")

        res = a * b
        print("\nРезультат: ", res)

        try:
            vihod = int(input("\n\nДля выхода введите 0: "))

            while vihod != 0:
                vihod = int(input("\n\nДля выхода введите 0: "))

            calc()
        except:
            print("\n\t\tОШИБКА! ВЫХОД В ГЛАВНОЕ МЕНЮ\n\n")
            main()

    if deistvie_calc == 4:
        a = 0
        b = 0

        while True:
            try:
                a = float(input("\n\nВведите первое число: "))
                b = float(input("\nВведите второе число: "))
                break
            except:
                print("\n\n\tОШИБКА (введите цифру!)\t")

        res = a / b
        print("\nРезультат: ", res)

        try:
            vihod = int(input("\n\nДля выхода введите 0: "))

            while vihod != 0:
                vihod = int(input("\n\nДля выхода введите 0: "))

            calc()
        except:
            print("\n\t\tОШИБКА! ВЫХОД В ГЛАВНОЕ МЕНЮ\n\n")
            main()

    if deistvie_calc == 5:
        a = 0
        b = 0

        while True:
            try:
                a = float(input("\n\nВведите первое число: "))
                b = float(input("\nВведите второе число: "))
                break
            except:
                print("\n\n\tОШИБКА (введите цифру!)\t")

        res = a // b
        print("\nРезультат: ", res)

        try:
            vihod = int(input("\n\nДля выхода введите 0: "))

            while vihod != 0:
                vihod = int(input("\n\nДля выхода введите 0: "))

            calc()
        except:
            print("\n\t\tОШИБКА! ВЫХОД В ГЛАВНОЕ МЕНЮ\n\n")
            main()

    if deistvie_calc == 6:
        a = 0
        b = 0

        while True:
            try:
                a = float(input("\n\nВведите первое число: "))
                b = float(input("\nВведите второе число: "))
                break
            except:
                print("\n\n\tОШИБКА (введите цифру!)\t")

        res = a % b
        print("\nРезультат: ", res)

        try:
            vihod = int(input("\n\nДля выхода введите 0: "))

            while vihod != 0:
                vihod = int(input("\n\nДля выхода введите 0: "))

            calc()
        except:
            print("\n\t\tОШИБКА! ВЫХОД В ГЛАВНОЕ МЕНЮ\n\n")
            main()

    if deistvie_calc == 7:
        a = 0
        b = 0

        while True:
            try:
                a = float(input("\n\nВведите первое число: "))
                b = float(input("\nВведите второе число: "))
                break
            except:
                print("\n\n\tОШИБКА (введите цифру!)\t")

        res = a ** b
        print("\nРезультат: ", res)

        try:
            vihod = int(input("\n\nДля выхода введите 0: "))

            while vihod != 0:
                vihod = int(input("\n\nДля выхода введите 0: "))

            calc()
        except:
            print("\n\t\tОШИБКА! ВЫХОД В ГЛАВНОЕ МЕНЮ\n\n")
            main()

    if deistvie_calc == 8:
        a = 0

        while True:
            try:
                a = float(input("\n\nВведите число: "))
                break
            except:
                print("\n\n\tОШИБКА (введите вещественное число!)\t")

        res = math.ceil(a)
        print("\nРезультат: ", res)

        try:
            vihod = int(input("\n\nДля выхода введите 0: "))

            while vihod != 0:
                vihod = int(input("\n\nДля выхода введите 0: "))

            calc()
        except:
            print("\n\t\tОШИБКА! ВЫХОД В ГЛАВНОЕ МЕНЮ\n\n")
            main()

    if deistvie_calc == 9:
        a = 0

        while True:
            try:
                a = float(input("\n\nВведите число: "))
                break
            except:
                print("\n\n\tОШИБКА (введите цифру!)\t")

        res = math.fabs(a)
        print("\nРезультат: ", res)

        try:
            vihod = int(input("\n\nДля выхода введите 0: "))

            while vihod != 0:
                vihod = int(input("\n\nДля выхода введите 0: "))

            calc()
        except:
            print("\n\t\tОШИБКА! ВЫХОД В ГЛАВНОЕ МЕНЮ\n\n")
            main()

    if deistvie_calc == 10:
        a = 0

        while True:
            try:
                a = int(input("\n\nВведите число: "))
                break
            except:
                print("\n\n\tОШИБКА (введите целое число!)\t")

        res = math.factorial(a)
        print("\nРезультат: ", res)

        try:
            vihod = int(input("\n\nДля выхода введите 0: "))

            while vihod != 0:
                vihod = int(input("\n\nДля выхода введите 0: "))

            calc()
        except:
            print("\n\t\tОШИБКА! ВЫХОД В ГЛАВНОЕ МЕНЮ\n\n")
            main()

    if deistvie_calc == 11:
        a = 0

        while True:
            try:
                a = float(input("\n\nВведите число: "))
                break
            except:
                print("\n\n\tОШИБКА (введите вещественное число!)\t")

        res = math.floor(a)
        print("\nРезультат: ", res)

        try:
            vihod = int(input("\n\nДля выхода введите 0: "))

            while vihod != 0:
                vihod = int(input("\n\nДля выхода введите 0: "))

            calc()
        except:
            print("\n\t\tОШИБКА! ВЫХОД В ГЛАВНОЕ МЕНЮ\n\n")
            main()

    if deistvie_calc == 12:
        a = 0
        b = 0
        c = 0

        while True:
            try:
                a = float(input("\n\nДлина ребра а (фронтальное): "))
                b = float(input("\nДлина ребра b (боковое): "))
                c = float(input("\nДлина ребра c (высота): "))
                break
            except:
                print("\n\n\tОШИБКА (введите цифру!)\t")

        res = lambda a_, b_, c_: 2*(c*a + a*b + c*b)
        print("\nРезультат: ", res(a,b,c), "(м^2)")

        try:
            vihod = int(input("\n\nДля выхода введите 0: "))

            while vihod != 0:
                vihod = int(input("\n\nДля выхода введите 0: "))

            calc()
        except:
            print("\n\t\tОШИБКА! ВЫХОД В ГЛАВНОЕ МЕНЮ\n\n")
            main()

    if deistvie_calc == 13:
        a = 0
        b = 0
        c = 0

        while True:
            try:
                a = float(input("\n\nДлина ребра а (фронтальное): "))
                b = float(input("\nДлина ребра b (боковое): "))
                c = float(input("\nДлина ребра c (высота): "))
                break
            except:
                print("\n\n\tОШИБКА (введите цифру!)\t")

        res = lambda a_, b_, c_: 4*a_ + 4*b_ + 4*c_
        print("\nРезультат: ", res(a,b,c))

        try:
            vihod = int(input("\n\nДля выхода введите 0: "))

            while vihod != 0:
                vihod = int(input("\n\nДля выхода введите 0: "))

            calc()
        except:
            print("\n\t\tОШИБКА! ВЫХОД В ГЛАВНОЕ МЕНЮ\n\n")
            main()

def stroki():
    a = (input("\n\nВведите строку: "))

    res_sybmlos = len(a)
    res_probelov = len(a) - len(a.replace(" ", ""))

    start = -1
    res_zyapyatih = 0

    while True:
        start = a.find(',', start+1)
        if start == -1:
            break
        res_zyapyatih += 1

    print("\n\nКол-во символов: ", res_sybmlos)
    print("\n\nКол-во пробелов: ", res_probelov)
    print("\n\nКол-во запятых: ", res_zyapyatih)

    try:
        vihod = int(input("\n\nДля выхода введите 0: "))

        while vihod != 0:
            vihod = int(input("\n\nДля выхода введите 0: "))

        main()
    except:
        print("\n\t\tОШИБКА! ВЫХОД В ГЛАВНОЕ МЕНЮ\n\n")
        main()

def matrix():

    x = 0
    y = 0
    a = 0
    b = 0

    try:
        x = int(input("\n\nВведите кол-во столбцов: "))
        y = int(input("\nВведите кол-во строк: "))
        a = int(input("\nВведите начальное значение: "))
        b = int(input("\nВведите шаг (на сколько будет увеличиваться значение): "))
    except:
        print("\n\n\tОШИБКА (введите целые цифры!)\t")
        matrix()

    matrix_ = []

    for row in range(y):
        temp_matrix = []
        for column in range(x):
            temp_matrix.append(a)
            a += b
        matrix_.append(temp_matrix)

    print("\n\nВаша матрица: \n")

    for i in range(y):
        for j in range(x):
            print(matrix_[i][j], end=" ")
        print()

    try:
        vihod = int(input("\n\nДля выхода введите 0: "))

        while vihod != 0:
            vihod = int(input("\n\nДля выхода введите 0: "))

        main()
    except:
        print("\n\t\tОШИБКА! ВЫХОД В ГЛАВНОЕ МЕНЮ\n\n")
        main()

def main():
    print("\tДействия\n\n")
    print("1 - калькулятор\n")
    print("2 - подсчёт в строке\n")
    print("3 - матрица\n")
    print("0 - выход\n\n")

    try:
        deistvie = int(input("Выберите действие: "))
    except:
        print("\n\t\tОШИБКА!")
        deistvie = int(input("\nВыберите действие: "))

    if deistvie == 1:
        calc()

    elif deistvie == 2:
        stroki()

    elif deistvie == 3:
        matrix()

    elif deistvie == 0:
        exit()

    else:
        print("\n\t\tОШИБКА!\n\n")
        main()

main()